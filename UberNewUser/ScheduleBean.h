//
//  ScheduleBean.h
//
//  Created by Spextrum on 18/07/16.
//  Copyright © 2016 Jigs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ScheduleBean : NSObject
//"scheduleDate":"Jul 15, 2016","scheduleTime":"08:07 PM","pickupAddress":null,"id":"86"

@property NSString *scheduleId;
@property NSString *PickUpaddress;
@property NSString *ScheduleDate;
@property NSString *ScheduleTime;

@end

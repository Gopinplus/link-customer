//
//  ScheduleTableCell.m
//  Link
//
//  Created by Spextrum on 13/03/17.
//  Copyright © 2017 Jigs. All rights reserved.
//

#import "ScheduleTableCell.h"

@implementation ScheduleTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  PickUpVC.m
//  UberNewUser
//
//  Created by Elluminati - macbook on 27/09/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//

#import "PickUpVC.h"
#import "SWRevealViewController.h"
#import "AFNHelper.h"
#import "AboutVC.h"
#import "ContactUsVC.h"
#import "ProviderDetailsVC.h"
#import "CarTypeCell.h"
#import "UIImageView+Download.h"
#import "CarTypeDataModal.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "UberStyleGuide.h"
#import "EastimateFareVC.h"
#import "UIImageView+Download.h"
#import "UIView+Utils.h"
#import <GoogleMaps/GoogleMaps.h>
#import <QuartzCore/QuartzCore.h>
#import "AFHTTPRequestOperationManager.h"
#import "sbMapAnnotation.h"
#import <QuartzCore/QuartzCore.h>


@interface PickUpVC ()<SWRevealViewControllerDelegate>
{
    NSString *strForUserId,*strForUserToken,*strForLatitude,*strForLongitude,*strForRequestID,*strForDriverLatitude,*strForDriverLongitude,*strForTypeid,*strForCurLatitude,*strForCurLongitude,*strMinFare,*strPassCap,*strETA,*Referral,*dist_price,*time_price,*driver_id,*strMobileno;
    NSString  *str_price_per_unit_distance, *str_base_distance,*strPayment_Option, *strForDriverList;
    NSMutableArray *arrForInformation,*arrForApplicationType,*arrForAddress,*arrDriver,*arrType;
    NSMutableDictionary *driverInfo;
    GMSMapView *mapView_;
    BOOL is_paymetCard,is_Fare,is_vehicleselected,is_containsVehicle;
    NSUserDefaults *pref;
    NSString *strType;
    SWRevealViewController *revealViewController;
}

@end

@implementation PickUpVC

#pragma mark -
#pragma mark - Init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark -
#pragma mark - ViewLife Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // [[AppDelegate sharedAppDelegate] hideLoadingView];
//    self.navigationController.navigationBar.barTintColor = [UberStyleGuide colorDefault];
//    self.navigationController.navigationBar.translucent = NO;

    is_vehicleselected = false;
    [self checkRequestInProgress];
    Referral=@"";
    strForTypeid=@"0";
    strPayment_Option = @"1";
    self.btnCancel.hidden=YES;
    arrForAddress=[[NSMutableArray alloc]init];
    arrForApplicationType=[[NSMutableArray alloc]init];
    
    self.tableForCity.hidden=YES;
    self.viewForPreferral.hidden=YES;
    self.viewForReferralError.hidden=YES;
    is_Fare=NO;
    driverInfo=[[NSMutableDictionary alloc] init];
    pref=[NSUserDefaults standardUserDefaults];
    self.viewForDriver.hidden=YES;
    [self.img_driver_profile applyRoundedCornersFullWithColor:[UIColor whiteColor]];
    if(![[pref valueForKey:PREF_IS_REFEREE] boolValue])
    {
        self.viewForPreferral.hidden=NO;
        self.navigationController.navigationBarHidden=YES;
        self.btnMyLocation.hidden=YES;
        self.btnETA.hidden=YES;
    }
    else
    {
        // [self setTimerToCheckDriverStatus];
    }
    
    if([[pref valueForKey:PREF_IS_REFEREE] boolValue])
    {
        self.navigationController.navigationBarHidden=NO;
        [self getAllApplicationType];
        [super setNavBarTitle:TITLE_PICKUP];
        [self customSetup];
        [self checkForAppStatus];
        [self getPagesData];
        [self.paymentView setHidden:YES];
        if(is_Fare==NO)
        {
            self.viewETA.hidden=YES;
            self.viewForFareAddress.hidden=YES;
            // [self getProviders];
        }
        else
            
        {
            self.viewETA.hidden=NO;
            self.viewForFareAddress.hidden=YES;
            pref=[NSUserDefaults standardUserDefaults];
            self.lblFareAddress.text=[pref valueForKey:PRFE_FARE_ADDRESS];
            self.lblFare.text=[NSString stringWithFormat:@"₡ %@",[pref valueForKey:PREF_FARE_AMOUNT]];
            
            [self.btnFare setTitle:[NSString stringWithFormat:@"%@",[pref valueForKey:PRFE_FARE_ADDRESS]] forState:UIControlStateNormal];
            self.btnFare.titleLabel.numberOfLines=2;
            self.btnFare.titleLabel.lineBreakMode=NSLineBreakByWordWrapping;
        }
        [self cashBtnPressed:nil];
    }
    [self customFont];
    [self updateLocationManagerr];
    CLLocationCoordinate2D coordinate = [self getLocation];
    strForCurLatitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
    strForCurLongitude= [NSString stringWithFormat:@"%f", coordinate.longitude];
    
    strForLatitude=strForCurLatitude;
    strForLongitude=strForCurLatitude;
    [self getAddress];
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[strForCurLatitude doubleValue] longitude:[strForCurLongitude doubleValue] zoom:14];
    mapView_ = [GMSMapView mapWithFrame:CGRectMake(0, 0, self.viewGoogleMap.frame.size.width, self.viewGoogleMap.frame.size.height) camera:camera];
    mapView_.myLocationEnabled = NO;
    mapView_.delegate=self;
    [self.viewGoogleMap addSubview:mapView_];
    [self.view bringSubviewToFront:self.tableForCity];
    
    
    self.viewforPickupLabel.layer.borderWidth = 1;
    self.viewforPickupLabel.layer.borderColor = [[UIColor colorWithRed:200/255.0 green:200/255.0 blue:200/255.0 alpha:1.0] CGColor];
    self.viewforPickupLabel.layer.shadowOpacity = 0.5;
    self.viewforPickupLabel.layer.shadowOffset = CGSizeMake(-1, 1);
    self.viewforPickupLabel.layer.shadowRadius = 2;
    self.viewforPickupLabel.layer.shadowColor = [UIColor colorWithRed:199/255.0 green:202/255.0 blue:2047/255.0 alpha:1.0].CGColor;

 
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [self SetLocalization];
    [super viewWillAppear:animated];
    [revealViewController setDelegate:self];

    
}
-(void)viewDidAppear:(BOOL)animated
{
    self.viewForReferralError.hidden=YES;
    self.viewForDriver.hidden=YES;
    
    self.viewForMarker.center=CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2-40);
    self.ViewforETA.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2-95);
    self.ViewforETA.layer.cornerRadius = 3;
    self.ViewforETA.layer.masksToBounds = YES;
    pref=[NSUserDefaults standardUserDefaults];
    if([[pref valueForKey:PREF_IS_REFEREE] boolValue])
    {
        self.navigationController.navigationBarHidden=NO;
        // [self getAllApplicationType];
        [super setNavBarTitle:TITLE_PICKUP];
        [self customSetup];
        [self checkForAppStatus];
        [self getPagesData];
        [self.paymentView setHidden:YES];
        if(is_Fare==NO)
        {
            self.viewETA.hidden=YES;
            self.viewForFareAddress.hidden=YES;
            [self getProviders];
        }
        else
        {
            self.viewETA.hidden=NO;
            self.viewForFareAddress.hidden=YES;
            pref=[NSUserDefaults standardUserDefaults];
            self.lblFareAddress.text=[pref valueForKey:PRFE_FARE_ADDRESS];
            self.lblFare.text=[NSString stringWithFormat:@"₡ %@",[pref valueForKey:PREF_FARE_AMOUNT]];
            if(strETA.length>0)
            {
                self.lblETA.text=strETA;
            }
            else
            {
                self.lblETA.text=@"0 min";
            }
            
            if (is_containsVehicle)
                self.lblforETA.text = [NSString stringWithFormat:@"%@", strETA];
            else
                self.lblforETA.text = @"--";
            
            //self.lblETA.text= [NSString stringWithFormat:@"%@", strETA];
            
            [self.btnFare setTitle:[NSString stringWithFormat:@"%@",[pref valueForKey:PRFE_FARE_ADDRESS]] forState:UIControlStateNormal];
            self.btnFare.titleLabel.numberOfLines=2;
            self.btnFare.titleLabel.lineBreakMode= NSLineBreakByWordWrapping;
        }
        [self cashBtnPressed:nil];
    }
    
    //    self.bottomView.frame=CGRectMake(0, self.view.frame.size.height-120, self.bottomView.frame.size.width, 80);
    
    //[self performSegueWithIdentifier:SEGUE_TO_ACCEPT sender:self];
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    self.navigationController.navigationBarHidden=NO;
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    //self.viewETA.hidden=YES;
    is_Fare=NO;
    self.viewForFareAddress.hidden=YES;
    self.lblFare.text=[NSString stringWithFormat:@"₡ %@",strMinFare];
}
- (void)customSetup
{
    revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.panGuestureview addGestureRecognizer:revealViewController.panGestureRecognizer];
        [self.revealButtonItem addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
//        [self.navigationController.navigationBar addGestureRecognizer:revealViewController.panGestureRecognizer];
    }
}
-(void)SetLocalization
{
    [self.btnPickMeUp setTitle:NSLocalizedString(@"PICK ME UP", nil) forState:UIControlStateNormal];
    [self.btnFare setTitle:NSLocalizedString(@"GET FARE ESTIMATE", nil) forState:UIControlStateNormal];
    [self.btnFare setTitle:NSLocalizedString(@"GET FARE ESTIMATE", nil) forState:UIControlStateSelected];
    // [self.btnClose setTitle:NSLocalizedString(@"Close", nil) forState:UIControlStateNormal];
    // [self.btnClose setTitle:NSLocalizedString(@"Close", nil) forState:UIControlStateSelected];
    [self.btnPayCancel setTitle:NSLocalizedString(@"CANCEL", nil) forState:UIControlStateNormal];
    [self.btnPayCancel setTitle:NSLocalizedString(@"CANCEL", nil) forState:UIControlStateSelected];
    [self.btnPayRequest setTitle:NSLocalizedString(@"Request", nil) forState:UIControlStateNormal];
    [self.btnPayRequest setTitle:NSLocalizedString(@"Request", nil) forState:UIControlStateSelected];
    [self.btnSelService setTitle:NSLocalizedString(@"SELECT SERVICE YOU NEED", nil) forState:UIControlStateNormal];
    [self.btnSelService setTitle:NSLocalizedString(@"SELECT SERVICE YOU NEED", nil) forState:UIControlStateSelected];
    [self.bReferralSkip setTitle:NSLocalizedString(@"SKIP", nil) forState:UIControlStateNormal];
    [self.bReferralSkip setTitle:NSLocalizedString(@"SKIP", nil) forState:UIControlStateSelected];
    [self.bReferralSubmit setTitle:NSLocalizedString(@"ADD", nil) forState:UIControlStateNormal];
    [self.bReferralSubmit setTitle:NSLocalizedString(@"ADD", nil) forState:UIControlStateSelected];
    [self.btnCancel setTitle:NSLocalizedString(@"CANCEL", nil) forState:UIControlStateNormal];
    [self.btnCancel setTitle:NSLocalizedString(@"CANCEL", nil) forState:UIControlStateSelected];
    [self.btnRatecard setTitle:NSLocalizedString(@"RATE CARD", nil) forState:UIControlStateNormal];
    [self.btnRatecard setTitle:NSLocalizedString(@"RATE CARD", nil) forState:UIControlStateSelected];
    
    self.lETA.text=NSLocalizedString(@"ETA", nil);
    self.lMaxSize.text=NSLocalizedString(@"MAX SIZE", nil);
    self.lMinFare.text=NSLocalizedString(@"MIN FARE", nil);
    self.lSelectPayment.text=NSLocalizedString(@"Select Your Payment Type", nil);
    self.lRefralMsg.text=NSLocalizedString(@"Referral_Msg", nil);
    self.lRate_basePrice.text=NSLocalizedString(@"Base Price :", nil);
    self.lRate_distancecost.text=NSLocalizedString(@"Distance Cost :", nil);
    self.lRate_TimeCost.text=NSLocalizedString(@"Time Cost :", nil);
    self.lblRateCradNote.text=NSLocalizedString(@"Rate_card_note", nil);
    self.txtAddress.placeholder=NSLocalizedString(@"SEARCH", nil);
    self.txtPreferral.placeholder=NSLocalizedString(@"Enter Referral Code", nil);
    
    [self.btnBooknow.layer setCornerRadius:10.0];
    [self.btnBooklater.layer setCornerRadius:10.0];
    
    
    [self.viewForRideLater.layer setCornerRadius:5.0];
    [self.paymentView.layer setCornerRadius:5.0];
    
    [self.viewForRideLater.layer setMasksToBounds:YES];
    [self.paymentView.layer setMasksToBounds:YES];
    [self.dot1.layer setCornerRadius:5];
    [self.dot1.layer setMasksToBounds:YES];
    [self.dot2.layer setCornerRadius:5];
    [self.dot2.layer setMasksToBounds:YES];
    
    [self.btnRatecard.layer setCornerRadius:5.0];
    [self.btnFare.layer setCornerRadius:5.0];
    [self.btnRatecard.layer setMasksToBounds:YES];
    [self.btnFare.layer setMasksToBounds:YES];

    
    
}
#pragma mark-
#pragma mark-

-(void)customFont
{
    self.txtAddress.font=[UberStyleGuide fontRegular];
    self.txtDropoffAddress.font=[UberStyleGuide fontRegular];
    self.btnCancel=[APPDELEGATE setBoldFontDiscriptor:self.btnCancel];
    self.btnPickMeUp=[APPDELEGATE setBoldFontDiscriptor:self.btnPickMeUp];
    self.btnSelService=[APPDELEGATE setBoldFontDiscriptor:self.btnSelService];
    self.lRate_basePrice.font = [UberStyleGuide fontSemiBold:13.0f];
    self.lRate_distancecost.font = [UberStyleGuide fontSemiBold:13.0f];
    self.lRate_TimeCost.font = [UberStyleGuide fontSemiBold:13.0f];
    self.lblRate_BasePrice.font = [UberStyleGuide fontRegular:13.0f];
    self.lblRate_DistancePrice.font = [UberStyleGuide fontRegular:13.0f];
    self.lblRate_TimePrice.font = [UberStyleGuide fontRegular:13.0f];
}

#pragma mark -
#pragma mark - Location Delegate

-(CLLocationCoordinate2D) getLocation
{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
    CLLocation *location = [locationManager location];
    CLLocationCoordinate2D coordinate = [location coordinate];
    return coordinate;
}

-(void)updateLocationManagerr
{
    [locationManager startUpdatingLocation];
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate=self;
    locationManager.desiredAccuracy=kCLLocationAccuracyBest;
    locationManager.distanceFilter=kCLDistanceFilterNone;
    
#ifdef __IPHONE_8_0
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8")) {
        // Use one or the other, not both. Depending on what you put in info.plist
        //[self.locationManager requestWhenInUseAuthorization];
        [locationManager requestAlwaysAuthorization];
    }
#endif
    
    [locationManager startUpdatingLocation];
    
}
- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    strForCurLatitude=[NSString stringWithFormat:@"%f",newLocation.coordinate.latitude];
    strForCurLongitude=[NSString stringWithFormat:@"%f",newLocation.coordinate.longitude];
    // GMSCameraUpdate *updatedCamera = [GMSCameraUpdate setTarget:newLocation.coordinate zoom:14];
    //[mapView_ animateWithCameraUpdate:updatedCamera];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    
    
}

#pragma mark-
#pragma mark- Alert Button Clicked Event

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==100)
    {
        if (buttonIndex == 0)
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
            
        }
    }
    if(alertView.tag==911)
    {
        if (buttonIndex == 1)
        {
            NSString *phoneNumber = [@"tel://" stringByAppendingString:@"12345"];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
            
            
        }
    }
    if(alertView.tag==108)
    {
        if (buttonIndex == 1)
        {
            NSString *phoneNumber = [@"tel://" stringByAppendingString:@"12345"];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
            
            
        }
    }
    if(alertView.tag==101)
    {
        if (buttonIndex == 1)
        {
            NSString *phoneNumber = [@"tel://" stringByAppendingString:@"12345"];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
            
            
        }
    }
    
}


#pragma mark- Google Map Delegate

- (void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position
{
    strForLatitude=[NSString stringWithFormat:@"%f",position.target.latitude];
    strForLongitude=[NSString stringWithFormat:@"%f",position.target.longitude];
}

- (void) mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position
{
    
    // if([strForDriverList isEqualToString:@"No Service Provider found around you."])
    if(strForDriverList)
        NSLog(@"hello");
    else
    {
        [self getETA:[arrDriver objectAtIndex:0]];
    }
    [self getAddress];
    [self getProviders];
    
}
-(void)getAddress
{
    NSString *url = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/directions/json?origin=%f,%f&destination=%f,%f&sensor=false",[strForLatitude floatValue], [strForLongitude floatValue], [strForLatitude floatValue], [strForLongitude floatValue]];
    
    NSString *str = [NSString stringWithContentsOfURL:[NSURL URLWithString:url] encoding:NSUTF8StringEncoding error:nil];
    
    NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData: [str dataUsingEncoding:NSUTF8StringEncoding]
                                                         options: NSJSONReadingMutableContainers
                                                           error: nil];
    
    NSDictionary *getRoutes = [JSON valueForKey:@"routes"];
    NSDictionary *getLegs = [getRoutes valueForKey:@"legs"];
    NSArray *getAddress = [getLegs valueForKey:@"end_address"];
    if (getAddress.count!=0)
    {
        if ([[[getAddress objectAtIndex:0]objectAtIndex:0] isEqualToString:@"338, Nigeria"]) {
               self.txtAddress.text=@"";
        }
        else
            self.txtAddress.text=[[getAddress objectAtIndex:0]objectAtIndex:0];
    }
}

#pragma mark - SWRevealViewController Delegate Methods

- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position
{
    if(position == FrontViewPositionLeft) {
        [self.panGuestureview setHidden:YES];
    } else {
        [self.panGuestureview setHidden:NO];
    }
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    if(position == FrontViewPositionLeft) {
        [self.panGuestureview setHidden:YES];

    } else {
        [self.panGuestureview setHidden:NO];
    }
}


#pragma mark -
#pragma mark - Mapview Delegate
- (MKAnnotationView *) mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>) annotation
{
    MKAnnotationView *annot=[[MKAnnotationView alloc] init];
    
    SBMapAnnotation *temp=(SBMapAnnotation*)annotation;
    if (temp.yTag==1000)
    {
        annot.image=[UIImage imageNamed:@"pin_driver"];
    }
    if (temp.yTag==1001)
    {
        annot.image=[UIImage imageNamed:@"pin_client_org"];
        
    }
    
    return annot;

}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view
{
    
}

-(void)showMapCurrentLocatinn
{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
    CLLocation *location = [locationManager location];
    CLLocationCoordinate2D coordinate = [location coordinate];
    
    
    GMSCameraUpdate *updatedCamera = [GMSCameraUpdate setTarget:coordinate zoom:14];
    [mapView_ animateWithCameraUpdate:updatedCamera];
    
    [self getAddress];
}


#pragma mark -
#pragma mark - Memory Mgmt

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark- Searching Method

- (IBAction)Searching:(id)sender
{
    aPlacemark=nil;
    [placeMarkArr removeAllObjects];
    self.tableForCity.hidden=YES;
    //  CLGeocoder *geocoder;
    NSString *str= @"";
    if ([self.txtAddress isFirstResponder])
        str= self.txtAddress.text;
    else if ([self.txtDropoffAddress isFirstResponder])
        str= self.txtDropoffAddress.text;
    
    NSLog(@"%@",str);
    if(str == nil)
        self.tableForCity.hidden=YES;
    
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
    //[dictParam setObject:str forKey:PARAM_ADDRESS];
    [dictParam setObject:str forKey:@"input"]; // AUTOCOMPLETE API
    [dictParam setObject:@"false" forKey:@"sensor"]; // AUTOCOMPLETE API
    //    [dictParam setObject:GOOGLE_KEY forKey:PARAM_KEY];
    [dictParam setObject:Google_Map_API_Key forKey:PARAM_KEY];
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
    [afn getAddressFromGooglewAutoCompletewithParamData:dictParam withBlock:^(id response, NSError *error)
     {
         if(response)
         {
             //NSArray *arrAddress=[response valueForKey:@"results"];
             NSArray *arrAddress=[response valueForKey:@"predictions"]; //AUTOCOMPLTE API
             
             NSLog(@"AutoCompelete URL: = %@",[[response valueForKey:@"predictions"] valueForKey:@"description"]);
             
             if ([arrAddress count] > 0)
             {
                 self.tableForCity.hidden=NO;
                 
                 placeMarkArr=[[NSMutableArray alloc] initWithArray:arrAddress copyItems:YES];
                 //[placeMarkArr addObject:Placemark]; o
                 [self.tableForCity reloadData];
                 
                 if(arrAddress.count==0)
                 {
                     self.tableForCity.hidden=YES;
                 }
             }
             
         }
         
     }];
    
}

#pragma mark - Tableview Delegate

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if(cell == nil)
    {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    
    
    NSString *formatedAddress=[[placeMarkArr objectAtIndex:indexPath.row] valueForKey:@"description"]; // AUTOCOMPLETE API
    cell.backgroundColor = [UIColor lightGrayColor];
    // cell.lblTitle.text=currentPlaceMark.name;
    cell.textLabel.text=formatedAddress;
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    aPlacemark=[placeMarkArr objectAtIndex:indexPath.row];
    self.tableForCity.hidden=YES;
    // [self textFieldShouldReturn:nil];
    
    [self setNewPlaceData];
    
    
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return placeMarkArr.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


-(void)setNewPlaceData
{
    if ([self.txtAddress isFirstResponder]) {
        self.txtAddress.text = [NSString stringWithFormat:@"%@",[aPlacemark objectForKey:@"description"]];
        [self textFieldShouldReturn:self.txtAddress];
    }
    else if ([self.txtDropoffAddress isFirstResponder]) {
        self.txtDropoffAddress.text = [NSString stringWithFormat:@"%@",[aPlacemark objectForKey:@"description"]];
        [self textFieldShouldReturn:self.txtDropoffAddress];
        [self MarkDropOff];
    }
}

-(void)MarkDropOff{
    if ([self.txtDropoffAddress.text length]>0) {
        CLLocationCoordinate2D center;
        center=[self getLocationFromAddressString:self.txtDropoffAddress.text];
//        double  latFrom=center.latitude;
//        double  lonFrom=center.longitude;
        
//        CLLocationCoordinate2D currentOwner;
//        currentOwner.latitude=[strowner_lati doubleValue];
//        currentOwner.longitude=[strowner_longi doubleValue];
        
        
        GMSMarker *markerOwner = [[GMSMarker alloc] init];
        markerOwner.position = center;
        markerOwner.icon = [UIImage imageNamed:@"pin_destination"];
        markerOwner.map = mapView_;
    }
    else {
    }
}

#pragma mark -
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if([segue.identifier isEqualToString:SEGUE_ABOUT])
    {
        AboutVC *obj=[segue destinationViewController];
        obj.arrInformation=arrForInformation;
    }
    else if([segue.identifier isEqualToString:SEGUE_TO_ACCEPT])
    {
        ProviderDetailsVC *obj=[segue destinationViewController];
        obj.strForLatitude=strForLatitude;
        obj.strForLongitude=strForLongitude;
        obj.strForWalkStatedLatitude=strForDriverLatitude;
        obj.strForWalkStatedLongitude=strForDriverLongitude;
    }
    else if([segue.identifier isEqualToString:@"contactus"])
    {
        ContactUsVC *obj=[segue destinationViewController];
        obj.dictContent=sender;
    }
    else if ([segue.identifier isEqualToString:@"segueToEastimate"])
    {
        EastimateFareVC *obj=[segue destinationViewController];
        obj.strForLatitude=strForLatitude;
        obj.strForLongitude=strForLongitude;
        obj.strMinFare=strMinFare;
        obj.str_base_distance = str_base_distance;
        obj.str_price_per_unit_distance = str_price_per_unit_distance;
    }
}

-(void)goToSetting:(NSString *)str
{
    [self performSegueWithIdentifier:str sender:self];
}

#pragma mark -
#pragma mark - UIButton Action

- (IBAction)eastimateFareBtnPressed:(id)sender
{
    is_Fare=YES;
    self.viewForRateCard.hidden=YES;
    [[AppDelegate sharedAppDelegate]showHUDLoadingView:@"Loading..."];
    [self performSegueWithIdentifier:@"segueToEastimate" sender:nil];
    
}

- (IBAction)closeETABtnPressed:(id)sender
{
    self.viewETA.hidden=YES;
    self.viewForFareAddress.hidden=YES;
    self.lblFare.text=[NSString stringWithFormat:@"₡ %@",strMinFare];
    is_Fare=NO;
}

- (IBAction)RateCardBtnPressed:(id)sender
{
    self.viewForRateCard.hidden=NO;
}


- (IBAction)ETABtnPressed:(id)sender {
    if (strForTypeid==nil || [strForTypeid isEqualToString:@"0"])
    {
        [APPDELEGATE showToastMessage:NSLocalizedString(@"SELECT_TYPE", nil)];
        return;
    }
    else if (!is_containsVehicle) {
        [APPDELEGATE showToastMessage:NSLocalizedString(@"No hay vehículos disponibles en esta aérea", nil)];
        return;
    }
    self.viewETA.hidden=NO;
    self.viewForRateCard.hidden=YES;
    [self.btnFare setTitle:NSLocalizedString(@"GET FARE ESTIMATE", nil) forState:UIControlStateNormal];
    [self.btnFare setTitle:NSLocalizedString(@"GET FARE ESTIMATE", nil) forState:UIControlStateSelected];
}

- (IBAction)cashBtnPressed:(id)sender
{
    [self.btnCash setSelected:YES];
    [self.btnCard setSelected:NO];
    is_paymetCard=NO;
    strPayment_Option = @"1";
}

- (IBAction)cardBtnPressed:(id)sender
{
    [self.btnCash setSelected:NO];
    [self.btnCard setSelected:YES];
    is_paymetCard=YES;
    strPayment_Option = @"0";
}

- (IBAction)requestBtnPressed:(id)sender
{
    if([CLLocationManager locationServicesEnabled])
    {
        if ([strForTypeid isEqualToString:@"0"]||strForTypeid==nil)
        {
            strForTypeid=@"1";
        }
        if(![strForTypeid isEqualToString:@"0"])
        {
            if(((strForLatitude==nil)&&(strForLongitude==nil))
               ||(([strForLongitude doubleValue]==0.00)&&([strForLatitude doubleValue]==0)))
            {
                [APPDELEGATE showToastMessage:NSLocalizedString(@"NOT_VALID_LOCATION", nil)];
            }
            else
            {
                if([[AppDelegate sharedAppDelegate]connected])
                {
                    
                    [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"REQUESTING", nil)];
                    
                    pref=[NSUserDefaults standardUserDefaults];
                    strForUserId=[pref objectForKey:PREF_USER_ID];
                    strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
                    
                    CLLocationCoordinate2D center;
                    center=[self getLocationFromAddressString:self.txtDropoffAddress.text];
                    
                    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
                    [dictParam setValue:strForLatitude forKey:PARAM_LATITUDE];
                    [dictParam setValue:strForLongitude  forKey:PARAM_LONGITUDE];
                    if ([self.txtDropoffAddress.text length]>0) {
                        [dictParam setValue:[NSString stringWithFormat:@"%f",center.latitude] forKey:@"d_latitude"];
                        [dictParam setValue:[NSString stringWithFormat:@"%f",center.longitude]  forKey:@"d_longitude"];
                    }
                    
                    //[dictParam setValue:@"22.3023117"  forKey:PARAM_LATITUDE];
                    //[dictParam setValue:@"70.7969645"  forKey:PARAM_LONGITUDE];
                    [dictParam setValue:@"1" forKey:PARAM_DISTANCE];
                    [dictParam setValue:strForUserId forKey:PARAM_ID];
                    [dictParam setValue:strForUserToken forKey:PARAM_TOKEN];
                    [dictParam setValue:strForTypeid forKey:PARAM_TYPE];
                    if (is_paymetCard)
                    {
                        [dictParam setValue:@"0" forKey:PARAM_PAYMENT_OPT];
                    }
                    else
                    {
                        [dictParam setValue:@"1" forKey:PARAM_PAYMENT_OPT];
                    }
                    
                    
                    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
                    [afn getDataFromPath:FILE_CREATE_REQUEST withParamData:dictParam withBlock:^(id response, NSError *error)
                     {
                         [[AppDelegate sharedAppDelegate]hideLoadingView];
                         NSDictionary *jsonObject = (NSDictionary *) response;
                         NSLog(@"Response check %@",jsonObject);
                         self.paymentView.hidden=YES;
                         if (response)
                         {
                             if([[response valueForKey:@"success"]boolValue])
                             {
                                 NSLog(@"pick up......%@",response);
                                 if([[response valueForKey:@"success"]boolValue])
                                 {
                                     NSMutableDictionary *walker=[response valueForKey:@"walker"];
                                     [self showDriver:walker];
                                     pref=[NSUserDefaults standardUserDefaults];
                                     
                                     strForRequestID=[response valueForKey:@"request_id"];
                                     [pref setObject:strForRequestID forKey:PREF_REQ_ID];
                                     [self setTimerToCheckDriverStatus];
                                     
                                     [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"COTACCTING_SERVICE_PROVIDER", nil)];
                                     [self.btnCancel setHidden:NO];
                                     [self.viewForDriver setHidden:NO];
                                     [APPDELEGATE.window addSubview:self.btnCancel];
                                     [APPDELEGATE.window bringSubviewToFront:self.btnCancel];
                                     [APPDELEGATE.window addSubview:self.viewForDriver];
                                     [APPDELEGATE.window bringSubviewToFront:self.viewForDriver];
                                 }
                             }
                             else
                             {
                                 UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                                 [alert show];
                             }
                         }
                         
                         
                     }];
                }
                else
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
                    [alert show];
                }
            }
            
        }
        else
            [APPDELEGATE showToastMessage:NSLocalizedString(@"SELECT_TYPE", nil)];
    }
    else
    {
        UIAlertView *alertLocation=[[UIAlertView alloc]initWithTitle:@"" message:@"Please Enable location access from Setting -> Link -> Privacy -> Location services" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        alertLocation.tag=100;
        [alertLocation show];
        
    }
    
}

- (IBAction)cancelBtnPressed:(id)sender
{
    [self.paymentView setHidden:YES];
}

- (IBAction)CancelRideLaterBtnPressed:(id)sender
{
    [self.viewForRideLater setHidden:YES];
}

- (IBAction)pickMeUpBtnPressed:(id)sender
{
    if (strForTypeid==nil || [strForTypeid isEqualToString:@"0"])
    {
        [APPDELEGATE showToastMessage:NSLocalizedString(@"SELECT_TYPE", nil)];
        return;
    }
    else if (!is_containsVehicle) {
        [APPDELEGATE showToastMessage:NSLocalizedString(@"No hay vehículos disponibles en esta aérea", nil)];
        return;
    }
    [self.viewForRideLater setHidden:YES];
    [self.paymentView setHidden:NO];
    
    
}
- (IBAction)pickMeLaterBtnPressed:(id)sender
{
    if (strForTypeid==nil || [strForTypeid isEqualToString:@"0"])
    {
        [APPDELEGATE showToastMessage:NSLocalizedString(@"SELECT_TYPE", nil)];
        return;
    }
    else if (!is_containsVehicle) {
        [APPDELEGATE showToastMessage:NSLocalizedString(@"No hay vehículos disponibles en esta aérea", nil)];
        return;
    }
    [self.paymentView setHidden:YES];
    [self.viewForRideLater setHidden:NO];
}

-(IBAction)datePickerBtnAction:(id)sender
{
    [_viewDatePicker setHidden:NO];
    [_viewTimePicker setHidden:YES];
    
    //    _viewDatePicker = [[UIDatePicker alloc] init];
    _viewDatePicker.datePickerMode=UIDatePickerModeDate;
    self.viewForPicker.hidden=NO;
    //    _viewDatePicker.date=[NSDate date];
    [_viewDatePicker addTarget:self action:@selector(setDate:) forControlEvents:UIControlEventValueChanged];
}
-(IBAction)timePickerBtnAction:(id)sender
{
    [_viewDatePicker setHidden:YES];
    [_viewTimePicker setHidden:NO];
    //    _viewTimePicker = [[UIDatePicker alloc] init];
    _viewTimePicker.datePickerMode=UIDatePickerModeTime;
    self.viewForPicker.hidden=NO;
    //    _viewDatePicker.date=[NSDate date];
    [_viewTimePicker addTarget:self action:@selector(setTime:) forControlEvents:UIControlEventValueChanged];
}

-(IBAction)btnDonePicker:(id)sender {
    self.viewForPicker.hidden=YES;
}

-(void)setDate:(id)sender
{
    NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
    dateFormat.dateStyle=NSDateFormatterMediumStyle;
    [dateFormat setDateFormat:@"YYYY-MM-dd"];
    NSString *str=[NSString stringWithFormat:@"%@",[dateFormat  stringFromDate:_viewDatePicker.date]];
    [self.btnPickDate setTitle:str forState:UIControlStateNormal];
}
-(void)setTime:(id)sender
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm:SS"];
    NSString *Time = [dateFormatter stringFromDate:_viewTimePicker.date];
    [self.btnPickTime setTitle:Time forState:UIControlStateNormal];
}


- (IBAction)RequestRideLaterBtnPressed:(id)sender
{
    NSString *strCurrentTime;
    NSString *RideTime;
    if ([self.btnPickTime.currentTitle isEqualToString:@"Seleccione hora"]) {
        [APPDELEGATE showToastMessage:NSLocalizedString(@"Kindly select a time", nil)];
        return;
    }
    if ([self.btnPickDate.currentTitle isEqualToString:@"Seleccione fecha"]) {
        [APPDELEGATE showToastMessage:NSLocalizedString(@"Kindly select a date", nil)];
        return;
    }
    else {
        NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
        [dateFormat setDateFormat:@"YYYY-MM-dd"];
        NSDate *date = [dateFormat dateFromString:self.btnPickDate.currentTitle];
        NSString *strCurrentDate = [dateFormat stringFromDate:[NSDate date]];
        NSDate *currentdate = [dateFormat dateFromString:strCurrentTime];
        if ([currentdate compare:date] == NSOrderedDescending) {
            [APPDELEGATE showToastMessage:NSLocalizedString(@"Date should not be in past.", nil)];
            return;
        }
        else {
            NSDateFormatter *timeFormatter2 = [[NSDateFormatter alloc] init];
            
            [timeFormatter2 setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
            NSTimeZone *timeZone = [NSTimeZone localTimeZone];
            [timeFormatter2 setTimeZone:timeZone];
            NSDate *time=[timeFormatter2 dateFromString:[NSString stringWithFormat:@"%@ %@",self.btnPickDate.currentTitle,self.btnPickTime.currentTitle]];
            strCurrentTime = [timeFormatter2 stringFromDate:[NSDate date]];
            NSDate *currenttime = [timeFormatter2 dateFromString:strCurrentTime];
            RideTime = [timeFormatter2 stringFromDate:time];
            NSTimeInterval secondsBetween = [time timeIntervalSinceDate:currenttime];
            if ([currenttime compare:time] == NSOrderedDescending||[currenttime compare:time] == NSOrderedSame||secondsBetween/60 < 30) {
                [APPDELEGATE showToastMessage:NSLocalizedString(@"Time should be greater than 30 mins.", nil)];
                return;
            }
        }
    }
    
    if([CLLocationManager locationServicesEnabled])
    {
        if ([strForTypeid isEqualToString:@"0"]||strForTypeid==nil)
        {
            strForTypeid=@"1";
        }
        if(![strForTypeid isEqualToString:@"0"])
        {
            if(((strForLatitude==nil)&&(strForLongitude==nil))
               ||(([strForLongitude doubleValue]==0.00)&&([strForLatitude doubleValue]==0)))
            {
                [APPDELEGATE showToastMessage:NSLocalizedString(@"NOT_VALID_LOCATION", nil)];
            }
            else
            {
                if([[AppDelegate sharedAppDelegate]connected])
                {
                    [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"REQUESTING", nil)];
                    pref=[NSUserDefaults standardUserDefaults];
                    strForUserId=[pref objectForKey:PREF_USER_ID];
                    strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
                    strMobileno =[pref objectForKey:PREF_MOBILE_NO];
                    NSTimeZone *timeZone = [NSTimeZone localTimeZone];
                    NSString *tzName = [timeZone name];
                    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
                    [dictParam setValue:strForLatitude forKey:PARAM_PICKUP_LATITUDE];
                    [dictParam setValue:strForLongitude forKey:PARAM_PICKUP_LONGITUDE];
                    //                    [dictParam setValue:@"" forKey:PARAM_DROPOFF_LATITUDE];
                    //                    [dictParam setValue:@"" forKey:PARAM_DROPOFF_LONGITUDE];
                    [dictParam setValue:RideTime forKey:PARAM_SCHEDULE];
                    [dictParam setValue:tzName forKey:PARAM_USER_TIMEZONE];
                    [dictParam setValue:strForTypeid forKey:PARAM_TYPE_OF_CAR];
                    [dictParam setValue:strMobileno forKey:PARAM_MOBILE_NUMBER];
                    [dictParam setValue:self.txtAddress.text forKey:PARAM_PICKUP_LOCATION];
                    [dictParam setValue:self.txtDropoffAddress.text forKey:PARAM_DROP_OFF_LOCATION];
                    //                    [dictParam setValue:@"" forKey:PARAM_CUSTOM_ADDRESS];
                    //                    [dictParam setValue:@"" forKey:PARAM_NUMBER_OF_ADULTS];
                    //                    [dictParam setValue:@"" forKey:PARAM_NUMBER_OF_CHILDREN];
                    //                    [dictParam setValue:@"" forKey:PARAM_LUGGAGE_COUNT];
                    //                    [dictParam setValue:@"" forKey:PARAM_RIDE_COMMENT];
                    //                    [dictParam setValue:@"" forKey:PARAM_PICKUP_DETAILS];
                    //                    [dictParam setValue:@"" forKey:PARAM_DROP_OFF_DETAILS];
                    //                    [dictParam setValue:@"" forKey:PARAM_EXECUTIVE_USERID];
                    
                    //'YYYY-MM-DD HH:MM:SS'
                    
                    
                    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
                    [manager POST:@"http://www.taxiappz.com/link/dispatch/Booking/addSchedule" parameters:dictParam success:^(AFHTTPRequestOperation *operation, id responseObject) {
                        NSLog(@"JSON: %@", responseObject);
                        [[AppDelegate sharedAppDelegate]hideLoadingView];
                        [self.viewForRideLater setHidden:YES];
                        NSDictionary *result = (NSDictionary*)responseObject;
                        if ([[result objectForKey:@"success"] boolValue]) {
                            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:[NSString stringWithFormat:@"Su viaje se realizo con exito %@ Compruebe el estado de viaje en el menû de reserva",RideTime] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                            [alert show];
                        }
                        else {
                            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Could not schedule ride right now. kindly try again." delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                            [alert show];
                        }
                        
                    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                        NSLog(@"Error: %@", error);
                        
                    }];
                }
                else
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
                    [alert show];
                }
            }
            
        }
        else
            [APPDELEGATE showToastMessage:NSLocalizedString(@"SELECT_TYPE", nil)];
    }
    else
    {
        UIAlertView *alertLocation=[[UIAlertView alloc]initWithTitle:@"" message:@"Please Enable location access from Setting -> Link -> Privacy -> Location services" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        alertLocation.tag=100;
        [alertLocation show];
        
    }
    
}

- (IBAction)cancelReqBtnPressed:(id)sender
{
    if([CLLocationManager locationServicesEnabled])
    {
        if([[AppDelegate sharedAppDelegate]connected])
        {
            [[AppDelegate sharedAppDelegate]hideLoadingView];
            [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"CANCLEING", nil)];
            
            pref=[NSUserDefaults standardUserDefaults];
            strForUserId=[pref objectForKey:PREF_USER_ID];
            strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
            NSString *strReqId=[pref objectForKey:PREF_REQ_ID];
            
            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
            
            [dictParam setValue:strForUserId forKey:PARAM_ID];
            [dictParam setValue:strForUserToken forKey:PARAM_TOKEN];
            [dictParam setValue:strReqId forKey:PARAM_REQUEST_ID];
            
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [afn getDataFromPath:FILE_CANCEL_REQUEST withParamData:dictParam withBlock:^(id response, NSError *error)
             {
                 if (response)
                 {
                     if([[response valueForKey:@"success"]boolValue])
                     {
                         [timerForCheckReqStatus invalidate];
                         timerForCheckReqStatus=nil;
                         [[AppDelegate sharedAppDelegate]hideLoadingView];
                         self.btnCancel.hidden=YES;
                         self.viewForDriver.hidden=YES;
                         //[self.btnCancel removeFromSuperview];
                         [APPDELEGATE showToastMessage:NSLocalizedString(@"REQUEST_CANCEL", nil)];
                         
                     }
                     else
                     {}
                 }
                 
                 
             }];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
            [alert show];
        }
        
    }
    else
    {
        UIAlertView *alertLocation=[[UIAlertView alloc]initWithTitle:@"" message:@"Please Enable location access from Setting -> Link -> Privacy -> Location services" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        alertLocation.tag=100;
        [alertLocation show];
        
        
    }
}

- (IBAction)myLocationPressed:(id)sender
{
    if ([CLLocationManager locationServicesEnabled])
    {
        CLLocationCoordinate2D coor;
        coor.latitude=[strForCurLatitude doubleValue];
        coor.longitude=[strForCurLongitude doubleValue];
        GMSCameraUpdate *updatedCamera = [GMSCameraUpdate setTarget:coor zoom:14];
        [mapView_ animateWithCameraUpdate:updatedCamera];
        
        
    }
    else
    {
        UIAlertView *alertLocation=[[UIAlertView alloc]initWithTitle:@"" message:@"Please Enable location access from Setting -> Link -> Privacy -> Location services" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        alertLocation.tag=100;
        [alertLocation show];
    }
    
}
- (IBAction)selectServiceBtnPressed:(id)sender
{
    UIDevice *thisDevice=[UIDevice currentDevice];
    if(thisDevice.userInterfaceIdiom == UIUserInterfaceIdiomPhone)
    {
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        float closeY=(iOSDeviceScreenSize.height-self.btnSelService.frame.size.height);
        
        float openY=closeY-(self.bottomView.frame.size.height-self.btnSelService.frame.size.height);
        if (self.bottomView.frame.origin.y==closeY)
        {
            
            [UIView animateWithDuration:0.5 animations:^{
                
                self.bottomView.frame=CGRectMake(0, openY, self.bottomView.frame.size.width, self.bottomView.frame.size.height);
                
            } completion:^(BOOL finished)
             {
             }];
        }
        else
        {
            [UIView animateWithDuration:0.5 animations:^{
                
                self.bottomView.frame=CGRectMake(0, closeY, self.bottomView.frame.size.width, self.bottomView.frame.size.height);
            } completion:^(BOOL finished)
             {
             }];
        }
    }
}

-(IBAction)btnSOSAction:(id)sender {
    [self.viewForSOS setHidden:NO];
    [self.navigationController setNavigationBarHidden:YES];
    [self.viewForSOS setFrame:self.view.frame];

}

-(IBAction)btnSOSCancel:(id)sender {
    [self.viewForSOS setHidden:YES];
    [self.navigationController setNavigationBarHidden:NO];
    
}

-(IBAction)btnSOSPolice:(id)sender {
    UIAlertView *alertLocation=[[UIAlertView alloc]initWithTitle:@"SOS" message:@"Seguro que desea llamar Police - 911?" delegate:self
                                               cancelButtonTitle:@"Cancel"
                                               otherButtonTitles:@"OK", nil];
    alertLocation.tag=911;
    [alertLocation show];
}

-(IBAction)btnSOSAmbulance:(id)sender {
    UIAlertView *alertLocation=[[UIAlertView alloc]initWithTitle:@"SOS" message:@"Seguro que desea llamar Ambulance - 108?" delegate:self
                                               cancelButtonTitle:@"Cancel"
                                               otherButtonTitles:@"OK", nil];
    alertLocation.tag=108;
    [alertLocation show];
}

-(IBAction)btnSOSFireStation:(id)sender {
    UIAlertView *alertLocation=[[UIAlertView alloc]initWithTitle:@"SOS" message:@"Seguro que desea llamar Fire Station - 101?" delegate:self
                                               cancelButtonTitle:@"Cancel"
                                               otherButtonTitles:@"OK", nil];
    alertLocation.tag=101;
    [alertLocation show];
}

#pragma mark -
#pragma mark - Custom WS Methods

-(void)getAllApplicationType
{
    
    if([[AppDelegate sharedAppDelegate]connected])
    {
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        [dictParam setValue:strForLatitude forKey:@"user_lat"];
        [dictParam setValue:strForLongitude forKey:@"user_long"];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_APPLICATION_TYPE withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             if (response)
             {
                 if([[response valueForKey:@"success"]boolValue])
                 {
                     arrForApplicationType=[[NSMutableArray alloc]init];
                     NSMutableArray *arr=[[NSMutableArray alloc]init];
                     [arr addObjectsFromArray:[response valueForKey:@"types"]];
                     arrType=[response valueForKey:@"types"];
                     for(NSMutableDictionary *dict in arr)
                     {
                         CarTypeDataModal *obj=[[CarTypeDataModal alloc]init];
                         obj.id_=[dict valueForKey:@"id"];
                         obj.name=[dict valueForKey:@"name"];
                         obj.icon=[dict valueForKey:@"icon"];
                         obj.is_default=[dict valueForKey:@"is_default"];
                         obj.price_per_unit_time=[dict valueForKey:@"price_per_unit_time"];
                         obj.price_per_unit_distance=[dict valueForKey:@"price_per_unit_distance"];
                         obj.base_price=[dict valueForKey:@"base_price"];
                         obj.eta=[dict valueForKey:@"duration"];
                         obj.isSelected=NO;
                         [arrForApplicationType addObject:obj];
                     }
                     [self.collectionView reloadData];
                 }
                 //  [[AppDelegate sharedAppDelegate]hideLoadingView];
                 else
                 {}
             }
             
         }];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
    [self MarkDropOff];

}
-(void)setTimerToCheckDriverStatus
{
    if (timerForCheckReqStatus) {
        [timerForCheckReqStatus invalidate];
        timerForCheckReqStatus = nil;
    }
    
    timerForCheckReqStatus = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(checkForRequestStatus) userInfo:nil repeats:YES];
}
-(void)checkForAppStatus
{
    pref=[NSUserDefaults standardUserDefaults];
    //  [pref removeObjectForKey:PREF_REQ_ID];
    NSString *strReqId=[pref objectForKey:PREF_REQ_ID];
    
    if(strReqId!=nil)
    {
        [self checkForRequestStatus];
    }
    else
    {
        [self RequestInProgress];
    }
}

-(void)checkForRequestStatus
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        pref=[NSUserDefaults standardUserDefaults];
        strForUserId=[pref objectForKey:PREF_USER_ID];
        strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
        NSString *strReqId=[pref objectForKey:PREF_REQ_ID];
        
        NSString *strForUrl=[NSString stringWithFormat:@"%@?%@=%@&%@=%@&%@=%@",FILE_GET_REQUEST,PARAM_ID,strForUserId,PARAM_TOKEN,strForUserToken,PARAM_REQUEST_ID,strReqId];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:strForUrl withParamData:nil withBlock:^(id response, NSError *error)
         {
             if (response)
             {
                 
                 if([[response valueForKey:@"success"]boolValue] && [[response valueForKey:@"confirmed_walker"] integerValue]!=0)
                 {
                     NSLog(@"GET REQ--->%@",response);
                     NSString *strCheck=[response valueForKey:@"walker"];
                     
                     if(strCheck)
                     {
                         self.btnCancel.hidden=YES;
                         self.viewForDriver.hidden=YES;
                         //[self.btnCancel removeFromSuperview];
                         
                         [[AppDelegate sharedAppDelegate]hideLoadingView];
                         NSMutableDictionary *dictWalker=[response valueForKey:@"walker"];
                         strForDriverLatitude=[dictWalker valueForKey:@"latitude"];
                         strForDriverLongitude=[dictWalker valueForKey:@"longitude"];
                         if ([[response valueForKey:@"is_walker_rated"]integerValue]==1)
                         {
                             [pref removeObjectForKey:PREF_REQ_ID];
                             return ;
                         }
                         
                         ProviderDetailsVC *vcFeed = nil;
                         for (int i=0; i<self.navigationController.viewControllers.count; i++)
                         {
                             UIViewController *vc=[self.navigationController.viewControllers objectAtIndex:i];
                             if ([vc isKindOfClass:[ProviderDetailsVC class]])
                             {
                                 vcFeed = (ProviderDetailsVC *)vc;
                             }
                             
                         }
                         if (vcFeed==nil)
                         {
                             [timerForCheckReqStatus invalidate];
                             timerForCheckReqStatus=nil;
                             [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"PLEASE_WAIT", nil)];
                             [self performSegueWithIdentifier:SEGUE_TO_ACCEPT sender:self];
                         }else
                         {
                             [self.navigationController popToViewController:vcFeed animated:NO];
                         }
                     }
                     
                 }
                 if([[response valueForKey:@"confirmed_walker"] intValue]==0 && [[response valueForKey:@"status"] intValue]==1)
                 {
                     [[AppDelegate sharedAppDelegate]hideLoadingView];
                     [timerForCheckReqStatus invalidate];
                     timerForCheckReqStatus=nil;
                     pref=[NSUserDefaults standardUserDefaults];
                     [pref removeObjectForKey:PREF_REQ_ID];
                     
//                     [APPDELEGATE showToastMessage:NSLocalizedString(@"NO_WALKER", nil)];
                     [APPDELEGATE hideLoadingView];
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry!" message:NSLocalizedString(@"NO_WALKER", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                     [alert show];
                     self.btnCancel.hidden=YES;
                     self.viewForDriver.hidden=YES;
                     // [self.btnCancel removeFromSuperview];
                     // [self showMapCurrentLocatinn];
                     
                 }
                 else
                 {
                     driverInfo=[response valueForKey:@"walker"];
                     [self showDriver:driverInfo];
                 }
             }
             
             else
             {}
         }];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
}
/*
 -(void)checkDriverStatus
 {
 if([[AppDelegate sharedAppDelegate]connected])
 {
 // [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"COTACCTING_SERVICE_PROVIDER", nil)];
 
 pref=[NSUserDefaults standardUserDefaults];
 strForUserId=[pref objectForKey:PREF_USER_ID];
 strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
 NSString *strReqId=[pref objectForKey:PREF_REQ_ID];
 
 NSString *strForUrl=[NSString stringWithFormat:@"%@?%@=%@&%@=%@&%@=%@",FILE_GET_REQUEST,PARAM_ID,strForUserId,PARAM_TOKEN,strForUserToken,PARAM_REQUEST_ID,strReqId];
 
 AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
 [afn getDataFromPath:strForUrl withParamData:nil withBlock:^(id response, NSError *error)
 {
 if([[response valueForKey:@"success"]boolValue])
 {
 NSLog(@"GET REQ--->%@",response);
 NSString *strCheck=[response valueForKey:@"walker"];
 
 if(strCheck)
 {
 [timerForCheckReqStatus invalidate];
 timerForCheckReqStatus=nil;
 [[AppDelegate sharedAppDelegate]hideLoadingView];
 
 [self performSegueWithIdentifier:SEGUE_TO_ACCEPT sender:self];
 }
 [[AppDelegate sharedAppDelegate]hideLoadingView];
 
 
 }
 else
 {}
 }];
 }
 else
 {
 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Network Status" message:@"Sorry, network is not available. Please try again later." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
 [alert show];
 }
 }*/
-(void)checkRequestInProgress
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        pref=[NSUserDefaults standardUserDefaults];
        strForUserId=[pref objectForKey:PREF_USER_ID];
        strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
        
        NSString *strForUrl=[NSString stringWithFormat:@"%@?%@=%@&%@=%@",FILE_GET_REQUEST_PROGRESS,PARAM_ID,strForUserId,PARAM_TOKEN,strForUserToken];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:strForUrl withParamData:nil withBlock:^(id response, NSError *error)
         {
             [[AppDelegate sharedAppDelegate]hideLoadingView];
             if (response)
             {
                 if([[response valueForKey:@"success"]boolValue])
                 {
                     //                     NSMutableDictionary *charge_details=[response valueForKey:@"charge_details"];
                     //                     dist_price=[charge_details valueForKey:@"distance_price"];
                     //                      pref=[NSUserDefaults standardUserDefaults];
                     //                     [pref setObject:dist_price forKey:PRFE_PRICE_PER_DIST];
                     //                     time_price=[charge_details valueForKey:@"price_per_unit_time"];
                     //                     [pref setObject:[charge_details valueForKey:@"price_per_unit_time"] forKey:PRFE_PRICE_PER_TIME];
                     //                   //  [pref setObject:[response valueForKey:@"request_id"] forKey:PREF_REQ_ID];
                     //                     [pref synchronize];
                     //
                     //                     self.lblRate_DistancePrice.text=[NSString stringWithFormat:@"$ %@",dist_price];
                     //                     self.lblRate_TimePrice.text=[NSString stringWithFormat:@"$ %@",time_price];
                     //
                     //[self checkForRequestStatus];
                 }
                 else
                 {}
             }
         }];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
}
-(void)RequestInProgress
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        
        
        pref=[NSUserDefaults standardUserDefaults];
        strForUserId=[pref objectForKey:PREF_USER_ID];
        strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
        
        NSString *strForUrl=[NSString stringWithFormat:@"%@?%@=%@&%@=%@",FILE_GET_REQUEST_PROGRESS,PARAM_ID,strForUserId,PARAM_TOKEN,strForUserToken];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:strForUrl withParamData:nil withBlock:^(id response, NSError *error)
         {
             [[AppDelegate sharedAppDelegate]hideLoadingView];
             if (response)
             {
                 if([[response valueForKey:@"success"]boolValue])
                 {
                     pref=[NSUserDefaults standardUserDefaults];
                     //                     NSMutableDictionary *charge_details=[response valueForKey:@"charge_details"];
                     //                     dist_price=[charge_details valueForKey:@"distance_price"];
                     //                     [pref setObject:dist_price forKey:PRFE_PRICE_PER_DIST];
                     //                     time_price=[charge_details valueForKey:@"price_per_unit_time"];
                     //                     [pref setObject:[charge_details valueForKey:@"price_per_unit_time"] forKey:PRFE_PRICE_PER_TIME];
                     //                     self.lblRate_DistancePrice.text=[NSString stringWithFormat:@"$ %@",dist_price];
                     //                     self.lblRate_TimePrice.text=[NSString stringWithFormat:@"$ %@",time_price];
                     
                     [pref setObject:[response valueForKey:@"request_id"] forKey:PREF_REQ_ID];
                     [pref synchronize];
                     [self checkForRequestStatus];
                 }
                 else
                 {}
             }
             
             
         }];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
}

-(void)getPagesData
{
    pref=[NSUserDefaults standardUserDefaults];
    strForUserId=[pref objectForKey:PREF_USER_ID];
    strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
    
    if([[AppDelegate sharedAppDelegate]connected])
    {
        NSMutableString *pageUrl=[NSMutableString stringWithFormat:@"%@?%@=%@",FILE_PAGE,PARAM_ID,strForUserId];
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:pageUrl withParamData:nil withBlock:^(id response, NSError *error)
         {
             NSLog(@"Respond to Request= %@",response);
             [APPDELEGATE hideLoadingView];
             
             if (response)
             {
                 arrPage=[response valueForKey:@"informations"];
                 if([[response valueForKey:@"success"] intValue]==1)
                 {
                     //   [APPDELEGATE showToastMessage:@"Requset Accepted"];
                 }
             }
             
         }];
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
}

-(void)getProviders
{
    pref=[NSUserDefaults standardUserDefaults];
    strForUserId=[pref objectForKey:PREF_USER_ID];
    strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setValue:strForUserId forKey:PARAM_ID];
    [dictParam setValue:strForUserToken forKey:PARAM_TOKEN];
    [dictParam setValue:strForTypeid forKey:PARAM_TYPE];
    [dictParam setValue:strForLatitude forKey:@"usr_lat"];
    [dictParam setValue:strForLongitude forKey:@"user_long"];
    
    
    //    [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"GET_PROVIDER", nil)];
    if([[AppDelegate sharedAppDelegate]connected])
    {
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_GET_PROVIDERS withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             NSLog(@"Respond to Get Provider= %@",response);
             // [APPDELEGATE hideLoadingView];
             
             if (response)
             {
                 // [arrDriver removeAllObjects];
                 strForDriverList = [response valueForKey:@"error"];
                 arrDriver=[response valueForKey:@"walker_list"];
             }
             else
             {
                 arrDriver=[[NSMutableArray alloc] init];
             }
             [self showProvider];
             [self getAllApplicationType];
             
         }];
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];        [alert show];
    }
}
-(void)showProvider
{
    [mapView_ clear];
    BOOL is_first=YES;
    is_containsVehicle = NO;
    if (is_vehicleselected) {
        for (int i=0; i<arrDriver.count; i++)
        {
            NSDictionary *dict=[arrDriver objectAtIndex:i];
            strType=[NSString stringWithFormat:@"%@",[dict valueForKey:@"type"]];
            if ([strForTypeid isEqualToString:strType])
            {
                is_containsVehicle = YES;
                GMSMarker *driver_marker;
                driver_marker = [[GMSMarker alloc] init];
                driver_marker.position = CLLocationCoordinate2DMake([[dict valueForKey:@"latitude"]doubleValue],[[dict valueForKey:@"longitude"]doubleValue]);
                driver_marker.icon=[UIImage imageNamed:@"pin_driver"];
                driver_marker.map = mapView_;
                if (is_first)
                {
                    [self getETA:dict];
                    is_first=NO;
                }
            }
        }
    }
    else {
        for (int i=0; i<arrDriver.count; i++)
        {
            is_containsVehicle = YES;
            NSDictionary *dict=[arrDriver objectAtIndex:i];
            GMSMarker *driver_marker;
            driver_marker = [[GMSMarker alloc] init];
            driver_marker.position = CLLocationCoordinate2DMake([[dict valueForKey:@"latitude"]doubleValue],[[dict valueForKey:@"longitude"]doubleValue]);
            driver_marker.icon=[UIImage imageNamed:@"pin_driver"];
            driver_marker.map = mapView_;
            if (is_first)
            {
                [self getETA:dict];
                is_first=NO;
            }
        }
        
    }
    is_first=YES;
    
}

-(void)getETA:(NSDictionary *)dict
{
    CLLocationCoordinate2D scorr=CLLocationCoordinate2DMake([strForLatitude doubleValue], [strForLongitude doubleValue]);
    CLLocationCoordinate2D dcorr=CLLocationCoordinate2DMake([[dict valueForKey:@"latitude"]doubleValue], [[dict valueForKey:@"longitude"]doubleValue]);
    [self calculateRoutesFrom:scorr to:dcorr];
    
}

-(NSArray*) calculateRoutesFrom:(CLLocationCoordinate2D) f to: (CLLocationCoordinate2D) t {
    NSString* saddr = [NSString stringWithFormat:@"%f,%f", f.latitude, f.longitude];
    NSString* daddr = [NSString stringWithFormat:@"%f,%f", t.latitude, t.longitude];
    
    //    NSString* apiUrlStr = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/directions/json?origin=%@&destination=%@&key=%@",saddr,daddr,GOOGLE_KEY_NEW];
    NSString* apiUrlStr = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/directions/json?origin=%@&destination=%@&key=%@",saddr,daddr,Google_Map_Server_Key];
    
    NSURL* apiUrl = [NSURL URLWithString:apiUrlStr];
    
    NSError* error = nil;
    NSData *data = [[NSData alloc]initWithContentsOfURL:apiUrl];
    
    NSDictionary *json =[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    
    if ([[json objectForKey:@"status"]isEqualToString:@"REQUEST_DENIED"] || [[json objectForKey:@"status"] isEqualToString:@"OVER_QUERY_LIMIT"] || [[json objectForKey:@"status"] isEqualToString:@"ZERO_RESULTS"])
    {
        
    }
    else
    {
        NSDictionary *getRoutes = [json valueForKey:@"routes"];
        NSDictionary *getLegs = [getRoutes valueForKey:@"legs"];
        NSArray *getAddress = [getLegs valueForKey:@"duration"];
        if(getAddress.count>0)
        {
            strETA = [[[getAddress objectAtIndex:0]objectAtIndex:0] valueForKey:@"text"];
            self.lblETA.text= [NSString stringWithFormat:@"%@", strETA];
        }
        else
            
        {
            self.lblETA.text=@"0 min";
            
        }
        
        if (is_containsVehicle)
            self.lblforETA.text = [NSString stringWithFormat:@"%@", strETA];
        else
            self.lblforETA.text = @"--";
        
        
        
    }
    return nil;
}
-(void)showDriver:(NSMutableDictionary *)walker
{
    if([driver_id integerValue]!=[[walker valueForKey:@"id"]integerValue ])
        
        //if(![driver_id isEqualToString:[NSString stringWithFormat:@"%@", [walker valueForKey:@"id"]]])
    {
        driver_id=[walker valueForKey:@"id"];
        self.lbl_driverName.text=[NSString stringWithFormat:@"%@ %@",[walker valueForKey:@"first_name"],[walker valueForKey:@"last_name"]];
        self.lbl_driverRate.text=[NSString stringWithFormat:@"%@", [walker valueForKey:@"rating"]];
        self.lbl_driver_Carname.text=[NSString stringWithFormat:@"%@",[walker valueForKey:@"car_model"]];
        self.lbl_driver_CarNumber.text=[NSString stringWithFormat:@"%@",[walker valueForKey:@"car_number"]];
        [self.img_driver_profile downloadFromURL:[walker valueForKey:@"picture"] withPlaceholder:nil];
    }
}

#pragma mark - UICollectionViewDataSource

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
//    NSLog(@"test width %f",self.view.frame.size.width);
//    if(arrForApplicationType.count==1)
//        return UIEdgeInsetsMake(0, (self.view.frame.size.width/2)-35, 0, 0);
//    else if(arrForApplicationType.count==2)
        return UIEdgeInsetsMake(0, 0, 0, 0);
//    else if(arrForApplicationType.count==3)
//        return UIEdgeInsetsMake(0, (self.view.frame.size.width/2)-105, 0, 0);
//    else if(arrForApplicationType.count==4)
//        return UIEdgeInsetsMake(0, (self.view.frame.size.width/2)-140, 0, 0);
//    else
//        return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrForApplicationType.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((CGRectGetWidth(self.view.frame))/2-7, (CGRectGetHeight(collectionView.frame)));
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CarTypeCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cartype" forIndexPath:indexPath];
    
//    cell.layer.borderWidth=1.0f;
//    cell.layer.borderColor=[UIColor colorWithRed:230/255.0 green:230/255.0 blue:230/255.0 alpha:1.0].CGColor;
    cell.backgroundColor = [UIColor whiteColor];
    cell.layer.masksToBounds = YES;
    cell.layer.cornerRadius =10;
    
    CarTypeDataModal *data = [arrForApplicationType objectAtIndex:indexPath.row];
    
    if (data.icon==nil || [data.icon isKindOfClass:[NSNull class]]){
        cell.imgType.image=[UIImage imageNamed:@"button_limo.png"];
    }
    else{
        if ([data.icon isEqualToString:@""]) {
            cell.imgType.image=[UIImage imageNamed:@"button_limo.png"];
        }
        else{
            [cell.imgType downloadFromURL:data.icon withPlaceholder:nil];
        }
    }
    cell.lblTitle.text=data.name;
    cell.lblETA.text = data.eta;
    if ([data.id_ isEqualToString:strForTypeid]) {
        NSLog(@"selection testing %ld",(long)indexPath.row);
        if ((long)indexPath.row==0)
            cell.backgroundColor= [UIColor colorWithRed:204/255.0 green:201/255.0 blue:201/255.0 alpha:1.0];
        else
            cell.backgroundColor= [UIColor colorWithRed:204/255.0 green:201/255.0 blue:201/255.0 alpha:1.0];
        
        [cell.lblTitle setTextColor:[UIColor whiteColor]];
        [cell.lblETA setTextColor:[UIColor whiteColor]];
    }
    else {
        [cell.lblTitle setTextColor:[UIColor blackColor]];
        [cell.lblETA setTextColor:[UIColor redColor]];
    }
    

    
//    [cell setCellData:[arrForApplicationType objectAtIndex:indexPath.row]];
    return cell;

    /*
     NSDictionary *dictType=[arrForApplicationType objectAtIndex:indexPath.row];
     if (strForTypeid==nil || [strForTypeid isEqualToString:@"0"])
     {
     if ([[dictType valueForKey:@"is_default"]intValue]==1)
     {
     for(CarTypeDataModal *obj in arrForApplicationType)
     {
     obj.isSelected = NO;
     }
     CarTypeDataModal *obj=[arrForApplicationType objectAtIndex:indexPath.row];
     obj.isSelected = YES;
     [self getETA:[arrDriver objectAtIndex:0]];
     NSDictionary  *dict=[arrType objectAtIndex:indexPath.row];
     //strMinFare=[NSString stringWithFormat:@"%@",[dict valueForKey:@"min_fare"]];
     strMinFare=[NSString stringWithFormat:@"%@",[dict valueForKey:@"base_price"]];
     strPassCap=[NSString stringWithFormat:@"%@",[dict valueForKey:@"max_size"]];
     str_base_distance = [NSString stringWithFormat:@"%@",[dict valueForKey:@"base_distance"]];
     str_price_per_unit_distance =  [NSString stringWithFormat:@"%f",[[dict valueForKey:@"price_per_unit_distance"  ] floatValue]];
     if(strETA.length>0)
     {
     self.lblETA.text=strETA;
     }
     else
     {
     self.lblETA.text=@"0 min";
     }
     
     self.lblFare.text=[NSString stringWithFormat:@"$ %@",strMinFare];
     self.lblRate_BasePrice.text=[NSString stringWithFormat:@"$%ld for %@ %@",(long)[strMinFare integerValue],[dict valueForKey:@"base_distance"],[dict valueForKey:@"unit"]];
     self.lblRate_DistancePrice.text = [NSString stringWithFormat:@"$%ld / %@",(long)[[dict valueForKey:@"price_per_unit_distance"] integerValue],[dict valueForKey:@"unit"]];
     NSString *strMin = @"min";
     self.lblRate_TimePrice.text = [NSString stringWithFormat:@"$%ld / %@",(long)[[dict valueForKey:@"price_per_unit_time"] integerValue],strMin];
     self.lblCarType.text=obj.name;
     self.lblSize.text=[NSString stringWithFormat:@"%@ PERSONS",strPassCap];
     strForTypeid=[NSString stringWithFormat:@"%@",obj.id_];
     pref=[NSUserDefaults standardUserDefaults];
     [pref setObject:strMinFare forKey:PREF_FARE_AMOUNT];
     [pref synchronize];
     }
     }
     */
    
    //  cell.imgType.layer.masksToBounds = YES;
    //   cell.imgType.layer.opaque = NO;
    //    cell.imgType.layer.cornerRadius=18;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    is_containsVehicle = NO;
    CarTypeDataModal *objPattern=[arrForApplicationType objectAtIndex:indexPath.row];
    NSString *strTypeid=[NSString stringWithFormat:@"%@",objPattern.id_];
    
    for (int i=0; i<arrDriver.count; i++)
    {
        NSDictionary *dict=[arrDriver objectAtIndex:i];
        strType=[NSString stringWithFormat:@"%@",[dict valueForKey:@"type"]];
        if ([strTypeid isEqualToString:strType])
        {
            is_containsVehicle = YES;
        }
    }
    
    
//    if (!is_containsVehicle)
//    {
//        return;
//    }
    
    is_vehicleselected = true;
    for(CarTypeDataModal *obj in arrForApplicationType) {
        obj.isSelected = NO;
    }
    CarTypeDataModal *obj=[arrForApplicationType objectAtIndex:indexPath.row];
    strForTypeid=[NSString stringWithFormat:@"%@",obj.id_];
    for (int i=0; i<arrDriver.count; i++)
    {
        NSDictionary *dict=[arrDriver objectAtIndex:i];
        strType=[NSString stringWithFormat:@"%@",[dict valueForKey:@"type"]];
        if ([strForTypeid isEqualToString:strType])
        {
            is_containsVehicle = YES;
        }
    }

    obj.isSelected = YES;
    NSDictionary *dict=[arrType objectAtIndex:indexPath.row];
    strMinFare=[NSString stringWithFormat:@"%@",[dict valueForKey:@"min_fare"]];
    strPassCap=[NSString stringWithFormat:@"%@",[dict valueForKey:@"max_size"]];
    str_base_distance = [NSString stringWithFormat:@"%@",[dict valueForKey:@"base_distance"]];
    //str_price_per_unit_distance =  [NSString stringWithFormat:@"%@",[dict valueForKey:@"price_per_unit_distance"]];
    str_price_per_unit_distance =  [NSString stringWithFormat:@"%f",[[dict valueForKey:@"price_per_unit_distance"  ] floatValue]];
    if ([strForTypeid intValue] !=[obj.id_ intValue])
    {
        // [self selectServiceBtnPressed:nil];
        if(strETA.length>0)
        {
            self.lblETA.text=strETA;
            NSLog(@"checking up ETA %@",strETA);
        }
        else
        {
            self.lblETA.text=@"0 min";
        }
        
        
        self.lblFare.text=[NSString stringWithFormat:@"₡ %@",strMinFare];
        self.lblRate_BasePrice.text=[NSString stringWithFormat:@"₡%ld for %@ %@",(long)[strMinFare integerValue],[dict valueForKey:@"base_distance"],[dict valueForKey:@"unit"]];
        self.lblRate_DistancePrice.text = [NSString stringWithFormat:@"₡%d / %@",[[dict valueForKey:@"price_per_unit_distance"] integerValue],[dict valueForKey:@"unit"]];
        NSString *strMin = @"min";
        self.lblRate_TimePrice.text = [NSString stringWithFormat:@"₡%d / %@",[[dict valueForKey:@"price_per_unit_time"] integerValue],strMin];
        self.lblCarType.text=obj.name;
        self.lblSize.text=[NSString stringWithFormat:@"%@ PERSONS",strPassCap];
        pref=[NSUserDefaults standardUserDefaults];
        
        [pref setObject:strMinFare forKey:PREF_FARE_AMOUNT];
        [pref synchronize];
    }
    strForTypeid=[NSString stringWithFormat:@"%@",obj.id_];
    
    [self showProvider];
    if (is_containsVehicle)
        self.lblforETA.text = [NSString stringWithFormat:@"%@", strETA];
    else
        self.lblforETA.text = @"--";
    
    [self.collectionView reloadData];
    [self setNewPlaceData];

}



#pragma mark
#pragma mark - UITextfield Delegate


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //NSString *strFullText=[NSString stringWithFormat:@"%@%@",textField.text,string];
    
    if(self.txtAddress==textField)
    {
        if(arrForAddress.count==1)
            self.tableView.frame=CGRectMake(self.tableView.frame.origin.x,86+134, self.tableView.frame.size.width, 44);
        else if(arrForAddress.count==2)
            self.tableView.frame=CGRectMake(self.tableView.frame.origin.x, 86+78, self.tableView.frame.size.width, 88);
        else if(arrForAddress.count==3)
            self.tableView.frame=CGRectMake(self.tableView.frame.origin.x, 86+34, self.tableView.frame.size.width, 132);
        else if(arrForAddress.count==0)
            self.tableView.hidden=YES;
        
        [self.tableView reloadData];
        
    }
    
    
    return YES;
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField==self.txtAddress)
    {
        self.txtAddress.text=@"";
        [self.tableForCity setFrame:CGRectMake(self.viewforPickupLabel.frame.origin.x+15, self.viewforPickupLabel.frame.origin.y+34, self.txtAddress.frame.size.width, 150)];
    }
    if(textField==self.txtDropoffAddress) {
        self.txtDropoffAddress.text=@"";
        [self.tableForCity setFrame:CGRectMake(self.viewforPickupLabel.frame.origin.x+15, self.viewforPickupLabel.frame.origin.y+70, self.txtDropoffAddress.frame.size.width, 150)];
    }
    if (textField==self.txtPreferral)
    {
        self.viewForReferralError.hidden=YES;
    }
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField==self.txtAddress)
    {
        [self getLocationFromString:self.txtAddress.text];
    }
    if(textField==self.txtDropoffAddress) {
        [self getLocationFromString:self.txtDropoffAddress.text];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    self.tableView.hidden=YES;
    
    // self.tableForCountry.frame=tempCountryRect;
    //  self.tblFilterArtist.frame=tempArtistRect;
    
    
    [textField resignFirstResponder];
    return YES;
}


-(void)getLocationFromString:(NSString *)str
{
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
    [dictParam setObject:str forKey:PARAM_ADDRESS];
    //    [dictParam setObject:GOOGLE_KEY forKey:PARAM_KEY];
    [dictParam setObject:Google_Map_API_Key forKey:PARAM_KEY];
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
    [afn getAddressFromGooglewithParamData:dictParam withBlock:^(id response, NSError *error)
     {
         if(response)
         {
             NSArray *arrAddress=[response valueForKey:@"results"];
             if ([arrAddress count] > 0)
                 
             {
                 self.txtAddress.text=[[arrAddress objectAtIndex:0] valueForKey:@"formatted_address"];
                 NSDictionary *dictLocation=[[[arrAddress objectAtIndex:0] valueForKey:@"geometry"] valueForKey:@"location"];
                 strForLatitude=[dictLocation valueForKey:@"lat"];
                 strForLongitude=[dictLocation valueForKey:@"lng"];
                 [self getETA:[arrDriver objectAtIndex:0]];
                 CLLocationCoordinate2D coor;
                 coor.latitude=[strForLatitude doubleValue];
                 coor.longitude=[strForLongitude doubleValue];
                 GMSCameraUpdate *updatedCamera = [GMSCameraUpdate setTarget:coor zoom:14];
                 [mapView_ animateWithCameraUpdate:updatedCamera];
                 // [self getProviders];
             }
         }
     }];
}

-(CLLocationCoordinate2D) getLocationFromAddressString: (NSString*) addressStr {
    double latitude = 0, longitude = 0;
    NSString *esc_addr =  [addressStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", esc_addr];
    NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
    if (result) {
        NSScanner *scanner = [NSScanner scannerWithString:result];
        if ([scanner scanUpToString:@"\"lat\" :" intoString:nil] && [scanner scanString:@"\"lat\" :" intoString:nil]) {
            [scanner scanDouble:&latitude];
            if ([scanner scanUpToString:@"\"lng\" :" intoString:nil] && [scanner scanString:@"\"lng\" :" intoString:nil]) {
                [scanner scanDouble:&longitude];
            }
        }
    }
    CLLocationCoordinate2D center;
    center.latitude=latitude;
    center.longitude = longitude;
    NSLog(@"View Controller get Location Logitute : %f",center.latitude);
    NSLog(@"View Controller get Location Latitute : %f",center.longitude);
    return center;
    
}

#pragma mark -
#pragma mark - Referral btn Action

- (IBAction)btnSkipReferral:(id)sender
{
    Referral=@"1";
    [self createService];
}

- (IBAction)btnAddReferral:(id)sender
{
    Referral=@"0";
    [self createService];
}

-(void)createService
{
    self.viewForReferralError.hidden=YES;
    if([[AppDelegate sharedAppDelegate]connected])
    {
        
        [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"REQUESTING", nil)];
        pref=[NSUserDefaults standardUserDefaults];
        
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        [dictParam setObject:[pref objectForKey:PREF_USER_ID] forKey:PARAM_ID];
        [dictParam setObject:self.txtPreferral.text forKey:PARAM_REFERRAL_CODE];
        [dictParam setObject:[pref objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
        [dictParam setObject:Referral forKey:PARAM_REFERRAL_SKIP];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_APPLY_REFERRAL withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             [[AppDelegate sharedAppDelegate]hideLoadingView];
             if (response)
             {
                 if([[response valueForKey:@"success"]boolValue])
                 {
                     NSLog(@"%@",response);
                     if([[response valueForKey:@"success"]boolValue])
                     {
                         pref=[NSUserDefaults standardUserDefaults];
                         [pref setObject:[response valueForKey:@"is_referee"] forKey:PREF_IS_REFEREE];
                         [pref synchronize];
                         self.viewForPreferral.hidden=YES;
                         self.btnMyLocation.hidden=NO;
                         self.btnETA.hidden=NO;
                         self.navigationController.navigationBarHidden=NO;
                         self.txtPreferral.text=@"";
                         if([Referral isEqualToString:@"0"])
                         {
                             [APPDELEGATE showToastMessage:[response valueForKey:@"error"]];
                         }
                         // [self setTimerToCheckDriverStatus];
                         self.navigationController.navigationBarHidden=NO;
                         [self getAllApplicationType];
                         [super setNavBarTitle:TITLE_PICKUP];
                         [self customSetup];
                         [self checkForAppStatus];
                         [self getPagesData];
                         [self getProviders];
                         [self.paymentView setHidden:YES];
                         self.viewETA.hidden=YES;
                         [self cashBtnPressed:nil];
                     }
                 }
                 else
                 {
                     self.txtPreferral.text=@"";
                     self.viewForReferralError.hidden=NO;
                     self.lblReferralMsg.text=[response valueForKey:@"error"];
                     self.lblReferralMsg.textColor=[UIColor colorWithRed:205.0/255.0 green:0.0/255.0 blue:15.0/255.0 alpha:1];
                 }
             }
             
             
         }];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
    
    
}

@end

